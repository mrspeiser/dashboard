$(document).ready(function(){
/* pagination */
console.log('testing');
var pagination = function(tblname) {
          $(tblname).each(function() {
          var currentPage = 0;
          var numPerPage = 10;
          var $table = $(this);
          $table.bind('repaginate', function() {
              $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
          });
          $table.trigger('repaginate');
          var numRows = $table.find('tbody tr').length;
          var numPages = Math.ceil(numRows / numPerPage);
          var $pager = $('<div class="pager"></div>');
          var pagecount = 0;
          for (var page = 0; page < numPages; page++) {
              $('<span class="page-number"></span>').text(page + 1).bind('click', {
                  newPage: page
              }, function(event) {
                  currentPage = event.data['newPage'];
                  $table.trigger('repaginate');
                  $(this).addClass('active').siblings().removeClass('active');
              }).appendTo($pager).addClass('clickable');
          pagecount++;    
          }
          if(pagecount > 1)
          {
          $pager.insertAfter($table.parents(".custom-table-view-scroll")).find('span.page-number:first').addClass('active');
          }
      });

}
pagination('table.custom-table-view');

    $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    })

/* show dropdown filter */
var countVisibleCheckbox = function(outerDiv){
  var total = $(outerDiv+' input:checkbox').length;
  var notChecked = $(outerDiv+' input:checkbox:not(":checked")').length;
  var totalChecked = total-notChecked;
  return totalChecked;
}

var col_select_li = '';
    $("table.custom-table-view span.th-name").each(function(){
      col_select_li += '<li><input type="checkbox" checked name="col_select_li" col-data-value="'+$(this).text().toLowerCase().replace(/ /g,"_")+'" value="'+$(this).text()+'">'+$(this).text()+'</li>';    
    });
$("div#custom-toolbar-select-col ul").html(col_select_li); 
$("table.custom-table-view th.column-title").each(function(){
$(this).attr("data-value",$(this).find('span').text().toLowerCase().replace(/ /g,"_"));
$(this).attr("data-length",$(this).index());
});
$("table.custom-table-view td").each(function(){
$(this).attr("data-length",$(this).index()); 
var th_data_index = "";
if($(this).attr("data-length") != 0){
th_data_index = $(this).attr("data-length");
}
$(this).attr("data-value",$("table.custom-table-view th.column-title").eq(th_data_index).text().toLowerCase().replace(/ /g,"_"));
});
$("[selectrow]").removeAttr("data-value");
/* code ending */
$("div#custom-toolbar-select-col input[type=checkbox]").click(function(){   
if($(this).prop("checked")){  
$('[data-value="'+$(this).attr("col-data-value")+'"]').show();
}
else
{
$('[data-value="'+$(this).attr("col-data-value")+'"]').hide();
}
$("span.total-col").text(countVisibleCheckbox('div#custom-toolbar-select-col'));
});

$("th[selectrow] input[type=checkbox]").click(function(){

    $("td[selectrow] input[type=checkbox]").prop("checked",$(this).prop("checked"));

});

/* start */
$("div#ash_tabs_links button .close_ash_tab").click(function(event){
event.preventDefault()
$(this).parents("button").remove(); 
opendashboard('campaign_dashboard.html');
return false;
});

/* start */
$("button#refresh").click(function(){
   $(".pager").show();
   $("div.pager span.active").click();
   $("input.search-box").val('');
});
/* end */
});