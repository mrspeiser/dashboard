(function(window) {
    function triggerCallback(e, callback) {
      if(!callback || typeof callback !== 'function') {
        return;
      }
      var files;
      if(e.dataTransfer) {
        files = e.dataTransfer.files;
      } else if(e.target) {
        files = e.target.files;
      }
      callback.call(null, files);
    }
    function makeDroppable(ele, callback) {
      var input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('multiple', true);
      input.style.display = 'none';
      input.addEventListener('change', function(e) {
        triggerCallback(e, callback);
      });
      ele.appendChild(input);
      
      ele.addEventListener('dragover', function(e) {
        e.preventDefault();
        e.stopPropagation();
        ele.classList.add('dragover');
      });

      ele.addEventListener('dragleave', function(e) {
        e.preventDefault();
        e.stopPropagation();
        ele.classList.remove('dragover');
      });

      ele.addEventListener('drop', function(e) {
        e.preventDefault();
        e.stopPropagation();
        ele.classList.remove('dragover');
        triggerCallback(e, callback);
      });
      
      ele.addEventListener('click', function() {
        input.value = null;
        input.click();
      });
    }
    window.makeDroppable = makeDroppable;
  })(this);
  (function(window) {
    makeDroppable(window.document.querySelector('.demo-droppable'), function(files) {
      console.log(files);
		 document.getElementById("upload_image").style.display = "block";
		 var element = document.querySelector('.upload_progress');		
		element.setAttribute('max',files.length);
      var output = document.querySelector('.output');
      //output.innerHTML = '';
	
      for(var i=0; i<files.length; i++) {
		  document.getElementById("upload_progress").value = i+1;
        if(files[i].type.indexOf('image/') === 0) {
			
          output.innerHTML += '<div class="output_div"><img width="200" src="' + URL.createObjectURL(files[i]) + '" /><p>'+files[i].name+'</p></div>';
		   //output.innerHTML += '<p>'+files[i].name+'</p></div>';
			setTimeout(function(){
				document.getElementById("upload_image").style.display = "none"; 
			}, 500);
			//document.getElementById("upload_image").style.display = "none";
        }
		 
        
		
      }
		
    });
  })(this);